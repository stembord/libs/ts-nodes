import * as tape from "tape";
import { Node } from "../lib";
import "./lesson/lib";
import "./wiki/lib";

tape("Node.parseName", (assert: tape.Test) => {
  const { organization, path, name } = Node.parseName(
    "stembord/wiki/probability-theory/sample-space"
  );
  assert.equals(organization, "stembord");
  assert.equals(path, "wiki/probability-theory");
  assert.equals(name, "sample-space");
  assert.end();
});

tape("parse lesson node", (assert: tape.Test) => {
  const node = Node.get("organization/path/lesson");
  assert.deepEquals(node, {
    organization: "organization",
    name: "lesson",
    path: "path",
    type: "lesson",
    tags: ["lesson"],
    data: node.data
  });
  assert.end();
});

tape("parse wiki node", (assert: tape.Test) => {
  const node = Node.get("organization/wiki");
  assert.deepEquals(node, {
    organization: "organization",
    name: "wiki",
    path: "",
    type: "wiki",
    tags: ["wiki"],
    data: node.data
  });
  assert.end();
});
