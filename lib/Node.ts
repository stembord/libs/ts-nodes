import { IJSONObject } from "@stembord/json";
import { isArray, isObject } from "util";

const NODE_NAME: string = "__STEMBORD_NODES__";
const NODE: INode = isObject((global as any)[NODE_NAME])
  ? (global as any)[NODE_NAME]
  : ((global as any)[NODE_NAME] = {});

export interface INode {
  [name: string]: Node;
}

export interface INewSecret {}

export const NEW_SECRET: INewSecret = {};

export enum NodeType {
  Lesson = "lesson",
  Wiki = "wiki"
}

export class Node {
  static create(json: IJSONObject, data: Record<string, any>) {
    const node = new Node(NEW_SECRET, json, data);
    NODE[node.getName()] = node;
    return node;
  }
  static get(name: string) {
    return NODE[name];
  }
  static parseName(
    fullName: string = ""
  ): { organization: string; path: string; name: string } {
    const paths = fullName.split("/"),
      organization = paths.shift() || "",
      name = paths.pop() || "",
      path = paths.join("/");

    return {
      organization,
      path,
      name
    };
  }

  organization: string;
  path: string;
  name: string;
  type: NodeType;
  tags: string[];
  data: Record<string, any>;

  constructor(
    newSecret: INewSecret,
    json: IJSONObject,
    data: Record<string, any>
  ) {
    if (newSecret !== NEW_SECRET) {
      throw new Error("Use Node.create(json: IJSONObject)");
    }

    const { organization, name, path } = Node.parseName(json.name as string);

    this.organization = organization;
    this.name = name;
    this.path = path;
    this.type = json.type as NodeType;
    this.tags = isArray(json.tags) ? (json.tags as string[]) : [];
    this.data = data;
  }

  getName() {
    const path = `${this.path ? `${this.path}/` : ""}${this.name}`;
    return `${this.organization}/${path}`;
  }
}
